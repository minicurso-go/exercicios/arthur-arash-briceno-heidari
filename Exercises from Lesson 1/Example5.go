/*
Faça um algoritmo que leia o valor do dólar em reais e um valor em dólares, e converta
esse valor para reais.
*/
package main

import "fmt"

func main() {

	var dolar float32
	var real float32
	const price = 5.01 // declaração de constante
	fmt.Println("Qual é o vfalor em dolar ?")
	fmt.Scan(&dolar)

	real = dolar * price
	fmt.Printf("R$: %.2f", real)
}
