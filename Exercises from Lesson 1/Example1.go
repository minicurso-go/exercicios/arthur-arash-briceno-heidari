/*
1) Faça um algoritmo que leia o nome,a idadee o peso de uma pessoa e imprima esses valores na tela.
*/
package main

import "fmt"

func main() {
	var name string
	var age int
	var peso float32
	fmt.Print("Hello, What's your name? ")
	fmt.Scan(&name)
	fmt.Print("How old are you, ", name, "? ")
	fmt.Scan(&age)
	fmt.Print("How much do you weigh, ", name, "? ")
	fmt.Scan(&peso)
	result := fmt.Sprintf("\n\nName: %s\nAge: %d\nPeso: %.2f\n", name, age, peso)
	fmt.Println(result)
}
