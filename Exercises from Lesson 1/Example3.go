/*
3)Faça um algoritmo que calcule o volume de uma caixa com base, altura e profundidade
informados pelo usuário.
*/
package main

import "fmt"

func main() {

	var altura, base, profundidade, volume float32

	fmt.Print("Qual é a altura do retângulo? ")
	fmt.Scan(&altura)
	fmt.Print("Qual é a base do retângulo? ")
	fmt.Scan(&base)
	fmt.Print("Qual é a base do profundidade? ")
	fmt.Scan(&profundidade)

	volume = base * altura * profundidade
	fmt.Println("\nÁrea do retângulo:", volume)
}
