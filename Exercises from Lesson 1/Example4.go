/*
4) Faça um algoritmo que calcule a média aritmética de quatro números informados pelo
usuário.
*/
package main

import "fmt"

func main() {
	var n1, n2 float32
	var media float32

	fmt.Println("Qual é n2?")
	fmt.Scan(&n1)
	fmt.Println("Qual é n1?")
	fmt.Scan(&n2)

	media = n1 + n2
	media /= 2
	fmt.Println("Media:", media)
}
/Users/arthurbriceno/go/src/awesomeProject/Aula_De_Go/Aula1