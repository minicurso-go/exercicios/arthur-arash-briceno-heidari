/*
2) Faça um algoritmo que calcule a área de um retângulo com base e altura informados
pelo usuário.
*/
package main

import "fmt"

func main() {
	var altura float32
	var base float32
	var area float32
	fmt.Println("Qual é a altura do retângulo?")
	fmt.Scan(&altura)
	fmt.Println("Qual é a base do retângulo?")
	fmt.Scan(&base)

	result := fmt.Sprintf("Altura: %f\nBase: %f\n", altura, base)
	fmt.Println(result)

	area = base * altura
	fmt.Println("Área do retângulo:", area)
}
