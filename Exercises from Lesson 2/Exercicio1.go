package main

import "fmt"

func main() {
    printCartesianPlane()
    var x, y float64
    fmt.Print("\nQual é o valor do X: ")
    fmt.Scan(&x)
    fmt.Print("Qual é o valor do Y: ")
    fmt.Scan(&y)

    if x == 0 && y == 0 {
        fmt.Println("Origem")
    } else if x == 0 {
        fmt.Println("Eixo Y")
    } else if y == 0 {
        fmt.Println("Eixo X")
    } else {
        if x > 0 {
            if y > 0 {
                fmt.Println("Esta na ((Q1))")
            } else {
                fmt.Println("Esta na ((Q4))")
            }
        } else {
            if y > 0 {
                fmt.Println("Esta na ((Q2))")
            } else {
                fmt.Println("Esta na ((Q3))")
            }
        }
    }
}

func printCartesianPlane() {
    fmt.Println("        ^ y")
    fmt.Println("        |")
    fmt.Println("    Q2  |  Q1")
    fmt.Println("        |")
    fmt.Println("----------------> x")
    fmt.Println("        |")
    fmt.Println("    Q3  |  Q4")
    fmt.Println("        |")
}
