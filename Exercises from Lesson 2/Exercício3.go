package main

import ("fmt")

func main() {
	var A, B, C float64

	fmt.Print("Digite o valor de A:")
	fmt.Scanln(&A)

	fmt.Print("Digite o valor de B:")
	fmt.Scanln(&B)

	fmt.Print("Digite o valor de C:")
	fmt.Scanln(&C)

	// Calcula area
	areaTriangulo := (A * C) / 2
	fmt.Printf("a) Área do triângulo retângulo: %.2f\n", areaTriangulo)

	areaCirculo := 3.14159 * (C * C)
	fmt.Printf("b) Área do círculo: %.2f\n", areaCirculo)

	areaTrapezio := ((A + B) * C) / 2
	fmt.Printf("c) Área do trapézio: %.2f\n", areaTrapezio)

	areaQuadrado := B * B
	fmt.Printf("d) Área do quadrado: %.2f\n", areaQuadrado)

	areaRetangulo := A * B
	fmt.Printf("e) Área do retângulo: %.2f\n", areaRetangulo)
}