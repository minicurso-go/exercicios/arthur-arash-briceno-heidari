package main

import ("fmt")

func main() {
	var n1, n2 float64
	var operacao int
    lerEntradas(&n1, &n2, &operacao)


	var resultado float64
	switch operacao {
	case 1:
		resultado = n1 + n2
	case 2:
		resultado = n1 - n2
	case 3:
		if n2 != 0 {
			resultado = n1 / n2
		} else {
			fmt.Println("Erro: Divisão por zero!")
			return
		}
	case 4:
		resultado = n1 * n2
	default:
		fmt.Println("Erro: Código de operação inválido!")
		return
	}

	fmt.Println("O resultado da operação é:", resultado)
	
}


func lerEntradas(operando1, operando2 *float64, operacao *int) {
	fmt.Print("Digite o primeiro número : ")
	fmt.Scanln(operando1)

	fmt.Print("Digite o segundo número : ")
	fmt.Scanln(operando2)

	fmt.Print(" \n|\t 1 - Soma\t\t\t | \n|\t 2 - Subtração\t\t | \n|\t 3 - Divisão\t\t |\n|\t 4 - Multiplicação\t | \n\nDigite o código da operação:")
	fmt.Scanln(operacao)
}
