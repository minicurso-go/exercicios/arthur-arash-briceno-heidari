package main

import "fmt"

func main() {
    var A, B, C, D int

    fmt.Print("Digite o valor de A:")
    fmt.Scan(&A)

    fmt.Print("Digite o valor de B:")
    fmt.Scan(&B)

    fmt.Print("Digite o valor de C:")
    fmt.Scan(&C)

    fmt.Print("Digite o valor de D:")
    fmt.Scan(&D)

    diferenca := ((A * B) - (C * D))

    fmt.Println("\nDiferença =", diferenca)
}

