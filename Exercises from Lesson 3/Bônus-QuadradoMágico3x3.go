package main

import "fmt"

func main() {
    var num int
    var sum1, sum2, sum3, sum4, sum5, sum6, sum7, sum8, sum9 int

    for i := 0; i < 9; i++ {
        fmt.Printf("Qual é o número %d? ", i+1) 
        fmt.Scan(&num)
        if i < 3 {
            sum1 += num
        }
        if i >= 3 && i < 6 {
            sum2 += num
        }
        if i >= 6 {
            sum3 += num
        }
        if i%3 == 0 {
            sum4 += num
        }
        if i%3 == 1 {
            sum5 += num
        }
        if i%3 == 2 {
            sum6 += num
        }
        if i == 0 || i == 4 || i == 8 {
            sum7 += num
        }
        if i == 2 || i == 4 || i == 6 {
            sum8 += num
        }
        if i == 0 || i == 3 || i == 6 {
            sum9 += num
        }
    }

    fmt.Printf("\n%d\t%d\t%d\n", sum1, sum2, sum3)
    fmt.Printf("%d\t%d\t%d\n", sum4, sum5, sum6)
    fmt.Printf("%d\t%d\t%d\n", sum7, sum8, sum9)


    if sum1 == sum2 && sum2 == sum3 && sum3 == sum4 && sum4 == sum5 && sum5 == sum6 && sum6 == sum7 && sum7 == sum8 && sum8 == sum9 {
        fmt.Println("\nSIM")
    } else {
        fmt.Println("\nNAO")
    }
}
