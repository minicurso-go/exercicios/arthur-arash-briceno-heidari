package main
import "fmt"
//Faça um algoritmo que leia um número inteiro positivo e mostre todos os seus divisores.

func main() {
    var num int
    fmt.Print("Qual é a numero? ")
    fmt.Scan(&num)
    
    for i := num; i > 0; i-- {
        if num%i==0{
            fmt.Printf("((%d)) e divi por ((%d)) \n ",i,num)
        }
}

}