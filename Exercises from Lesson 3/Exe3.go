package main
import "fmt"
// Faça um algoritmo que imprima a tabuada de multiplicação de 1 a 10 para um número fornecido pelo usuário.
func main() {
    var n int
    fmt.Print("Qual é a numero para fazer a tabuada?")
    fmt.Scan(&n)
    for i := 0; i <= 10; i++ {
        result := i*n
        fmt.Printf("%d x %d = %d\n",i,n , result )
        
}

}